import { Task } from '../models/task';

export const tasks: Task[] = [
  { description: "create todo app with angular", isImportant: true, isComplited: false },
  { description: "read TS documetation", isImportant: false, isComplited: false },
  { description: "create repository on GH", isImportant: false, isComplited: false }
]