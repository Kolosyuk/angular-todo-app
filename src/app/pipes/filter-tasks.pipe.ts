import { Pipe, PipeTransform } from '@angular/core';
import { Task } from '../models/task';

@Pipe({
  name: 'filterTasks'
})
export class FilterTasksPipe implements PipeTransform {

  transform(tasks: Task[], search: string): Task[] {
    if (search.length === 0) return tasks;
    return tasks.filter(p => p.description.toLowerCase().includes(search.toLowerCase()))
  }
}
