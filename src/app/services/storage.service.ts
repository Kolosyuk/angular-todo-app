import { Injectable } from '@angular/core';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  saveData(todos: Task[]): void {
    localStorage.setItem('todos', JSON.stringify(todos));
  };

  loadData() {
    let value = localStorage.getItem('todos');
    if (value != '' && value != null && typeof value != "undefined") {
      return JSON.parse(value);
    }
    return [];
  }
}
