import { Injectable } from '@angular/core';
import { Task } from '../models/task';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  constructor(
    private storageService: StorageService
  ) { }

  tasks: Task[] = [];

  getAllTasks() {
      this.tasks = this.storageService.loadData()
  }

  addTask(task:Task ) {
    this.tasks.push(task);
    this.storageService.saveData(this.tasks);
  };

  removeTask(id: number) {
    this.tasks = this.tasks.filter((_, idx) => idx !== id);
    this.storageService.saveData(this.tasks);
  };

  completeTask(id: number) {
    this.tasks = this.tasks.map((item, idx) => {
      return idx !== id ? item : {
        description: item.description,
        isImportant: item.isImportant,
        isComplited: true
      }
    });
    this.storageService.saveData(this.tasks);
    
  }

  removeAllTasks() {
    this.tasks.length = 0;
    this.storageService.saveData(this.tasks);
  }
}
