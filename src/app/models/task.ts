export interface Task {
  description: string;
  isImportant: boolean;
  isComplited: boolean;
}