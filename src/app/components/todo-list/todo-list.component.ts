import { Component, OnInit } from '@angular/core';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  
  constructor(
    public tasksService: TasksService
  ) {  };

  term = '';


  ngOnInit(): void {
    this.tasksService.getAllTasks();
  }

}
