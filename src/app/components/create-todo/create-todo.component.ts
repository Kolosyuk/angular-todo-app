import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from 'src/app/services/modal.service';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css']
})
export class CreateTodoComponent{

  constructor(
    public tasksService: TasksService,
    private modalService: ModalService
    ) {
    
  }

  form = new FormGroup({
    description: new FormControl<string>('', [
      Validators.required,
      Validators.minLength(4)
    ]),
    importance: new FormControl<boolean>(false)
  });

  get description() {
    return this.form.controls.description as FormControl
  }

  submit() {
    this.tasksService.addTask(
      {
        description: this.form.value.description as string,
        isImportant: this.form.value.importance as boolean,
        isComplited: false,
    });
    this.modalService.close();
  }

}
