import { Component } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { TasksService } from 'src/app/services/tasks.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  constructor(
    public tasksService: TasksService,
    public modalService: ModalService,
  ) {}
}
