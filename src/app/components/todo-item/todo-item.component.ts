import { Component, Input } from '@angular/core';
import { Task } from 'src/app/models/task';
import { TasksService } from '../../services/tasks.service';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent {
  @Input() task: Task
  @Input() id: number

  constructor(
    public tasksService: TasksService,
  ) {}
}
